import React from "react";
import { Cards, Chart, CountryPicker } from "./Components/Exporter.js";
import Classes from "./App.module.css";
import { fetchData } from "./api";
import covid19image from "./image.png";
export default class App extends React.Component {
	state = {
		data: {},
		country: "",
	};
	async componentDidMount() {
		const fetchedData = await fetchData();
		this.setState({ data: fetchedData });
	}
	handleCountryChange = async (country) => {
		try {
			const fetchedCountryData = await fetchData(country);
			this.setState({ data: fetchedCountryData, country: country });
		} catch (e) {
			console.log(e);
		}
	};

	render() {
		const { data, country } = this.state;
		return (
			<div className={Classes.container}>
				<img src={covid19image} alt="COVID-19-LOGO" className={Classes.image} />
				<Cards data={data} />
				<CountryPicker handleCountryChange={this.handleCountryChange} />
				<Chart data={data} country={country} />
			</div>
		);
	}
}
