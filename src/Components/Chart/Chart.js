import React, { useState, useEffect } from "react";
import { fetchDailyData } from "../../api";
import { Line, Bar } from "react-chartjs-2";
import Classes from "./Chart.module.css";
const Chart = ({ data: { confirmed, recovered, deaths }, country }) => {
	const [dailyData, setdailyData] = useState([]);

	useEffect(() => {
		const fetchAPI = async () => {
			setdailyData(await fetchDailyData());
		};
		fetchAPI();
	}, []);
	console.log(dailyData);
	const LineChart = dailyData.length ? (
		<Line
			data={{
				labels: dailyData.map(({ date }) => date),
				datasets: [
					{
						data: dailyData.map(({ confirmed }) => confirmed),
						label: "Infected",
						borderColor: "#3333ff",
						borderWidth: "5px",
						fill: true,
					},
					{
						data: dailyData.map(({ deaths }) => deaths),
						label: "Deaths",
						borderColor: "red",
						backgroundColor: "rgba(255,0,0,0.5)",
						borderWidth: "5px",
						fill: true,
					},
					{
						data: dailyData.map(({ recovered }) => recovered),
						label: "Recovered",
						borderColor: "green",
						borderWidth: "5px",
						fill: true,
					},
				],
			}}
		/>
	) : null;
	const barChart = confirmed ? (
		<Bar
			data={{
				labels: ["Infected", "Recovered", "Deaths"],
				datasets: [
					{
						label: "People",
						backgroundColor: [
							"rgba(0,0,255,0.5)",
							"rgba(0,255,0,0.5)",
							"rgba(255,0,0,0.5)",
						],
						data: [confirmed.value, recovered.value, deaths.value],
					},
				],
			}}
			options={{
				legend: { display: false },
				title: {
					display: true,
					text: `Current Display Country : ${country}`,
				},
			}}
		/>
	) : null;
	return (
		<div className={Classes.container}>{country ? barChart : LineChart}</div>
	);
};
export default Chart;
