import React, { useState, useEffect } from "react";
import { NativeSelect, FormControl } from "@material-ui/core";
import { fetchCountries } from "../../api";
import Classes from "./CountryPicker.module.css";
const CountryPicker = ({ handleCountryChange }) => {
	const [fetchedCountries, setFetchedCountries] = useState([]);
	useEffect(async () => {
		const fetchcountries = async () => {
			return await fetchCountries();
		};
		const response = await fetchCountries();
		const {
			data: { countries },
		} = response;
		setFetchedCountries(
			countries.map((obj, index) => {
				return (
					<option key={index} value={obj.name}>
						{obj.name}
					</option>
				);
			})
		);
	}, []);
	return (
		<FormControl className={Classes.FormControl}>
			<NativeSelect
				className={Classes.NativeSelector}
				defaultValue=""
				onChange={(e) => {
					handleCountryChange(e.target.value);
				}}
			>
				<option value="">Global</option>
				{fetchedCountries}
			</NativeSelect>
		</FormControl>
	);
};
export default CountryPicker;
