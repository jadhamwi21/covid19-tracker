import axios from "axios";

const url = "https://covid19.mathdro.id/api";

export const fetchData = async (country) => {
	let changable_url = url;
	if (country) {
		changable_url = `${url}/countries/${country}`;
	}
	try {
		const {
			data: { confirmed, recovered, deaths, lastUpdate },
		} = await axios.get(changable_url);
		return { confirmed, recovered, deaths, lastUpdate };
	} catch (error) {
		console.log(error);
	}
};
export const fetchDailyData = async () => {
	try {
		const { data } = await axios.get(
			"https://api.covidtracking.com/v1/us/daily.json"
		);
		const wantedData = data.map((dailydata) => {
			return {
				confirmed: dailydata.positive,
				deaths: dailydata.death,
				date: dailydata.date,
				recovered: dailydata.recovered,
			};
		});

		return wantedData;
	} catch (error) {
		console.log(error);
	}
};
export const fetchCountries = async () => {
	try {
		const response = await axios.get(`${url}/countries`);

		return response;
	} catch (error) {}
};
